import Vapor

/// Register your application's routes here.
public func routes(_ router: Router) throws {
    // Basic "Hello, world!" example
    router.get("hello") { req in
        return "Hello, world!"
    }
    
    //String.parameter -> especificar que o parametro é do tipo String
    router.get("hello", String.parameter) { req -> String in
        
        let name = try req.parameters.next(String.self); //o nome é o parametro do tipo String que recebemos acima
        
        return "hello, \(name)!";
    }
    
    
    //router.post(content(decodable), at: path) { request, requestDecodable -> requestCodable
    router.post(InfoData.self, at: "info") { req, data -> String in
        
        return "Hello, \(data.name)!"
    }
    
    router.post(InfoData.self, at: "info") { req, data -> InfoDataJSON in
        
        return InfoDataJSON(request: data);
    }

    
}

struct InfoData : Content
{
    let name : String;
}

struct InfoDataJSON : Content
{
    let request : InfoData;
}
